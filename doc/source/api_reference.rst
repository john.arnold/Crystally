=================
API Reference
=================

-----------------
Vectors
-----------------

.. currentmodule:: crystally.core.vectors

.. autoclass:: PeriodicVector
    :members:


.. currentmodule:: crystally.core.lattice

-----------------
Atom
-----------------

.. autoclass:: Atom
    :members:

-----------------
Lattice
-----------------

.. autoclass:: Lattice
    :members:

-----------------
File handling
-----------------

.. currentmodule:: crystally.core.file_handling

.. autofunction:: read_poscar

.. autofunction:: write_poscar
