=================
How do I ...
=================

---------------------
... install crystally?
---------------------

:mod:`crystally` is obtainable via pip. To install the package simply type:

.. code-block:: bash

    python3 -m pip install crystally [--user]

After this step you can import crystally:

.. code-block:: python

    import crystally

---------------------
... create a lattice?
---------------------

To create a lattice, you need to specify the cartesian lattice vectors and the atoms with their fractional coordinates.
An atom has three attributes: an element, a position and a sublattice. The last of these attributes is more of a
wildcard, to help with sorting or filtering operations.

The ceria system is then easily created by:

.. code-block:: python

    >>> from crystally import Lattice, Atom
    >>> ceria = Lattice(vectors = [[5.499, 0, 0], [0, 5.499, 0], [0, 0, 5.499]],
                        atoms = [Atom("Ce", [0, 0, 0], "cations"),
                                 Atom("Ce", [0, 0.5, 0.5], "cations"),
                                 Atom("Ce", [0.5, 0, 0.5], "cations"),
                                 Atom("Ce", [0.5, 0.5, 0], "cations"),
                                 Atom("O", [0.25, 0.25, 0.25], "anions"),
                                 Atom("O", [0.75, 0.25, 0.25], "anions"),
                                 Atom("O", [0.25, 0.75, 0.25], "anions"),
                                 Atom("O", [0.25, 0.25, 0.75], "anions"),
                                 Atom("O", [0.75, 0.75, 0.25], "anions"),
                                 Atom("O", [0.25, 0.75, 0.75], "anions"),
                                 Atom("O", [0.75, 0.25, 0.75], "anions"),
                                 Atom("O", [0.75, 0.75, 0.75], "anions")])

We will use this system in the following explanations.

-----------------------
... view the lattice?
-----------------------

To get a nicely readable representation of the lattice simply use the print command:

>>> print(ceria)
Lattice:
     Vectors:
           5.4994687617  0.0000000000  0.0000000000
           0.0000000000  5.4994687617  0.0000000000
           0.0000000000  0.0000000000  5.4994687617
     Positions:
          0    Atom: Ce    [ 0.0000000000,  0.0000000000,  0.0000000000]  cations
          1    Atom: Ce    [ 0.0000000000,  0.5000000000,  0.5000000000]  cations
          2    Atom: Ce    [ 0.5000000000,  0.0000000000,  0.5000000000]  cations
          3    Atom: Ce    [ 0.5000000000,  0.5000000000,  0.0000000000]  cations
          4    Atom: O     [ 0.2500000000,  0.2500000000,  0.2500000000]  anions
          5    Atom: O     [ 0.7500000000,  0.2500000000,  0.2500000000]  anions
          6    Atom: O     [ 0.2500000000,  0.7500000000,  0.2500000000]  anions
          7    Atom: O     [ 0.2500000000,  0.2500000000,  0.7500000000]  anions
          8    Atom: O     [ 0.7500000000,  0.7500000000,  0.2500000000]  anions
          9    Atom: O     [ 0.2500000000,  0.7500000000,  0.7500000000]  anions
          10   Atom: O     [ 0.7500000000,  0.2500000000,  0.7500000000]  anions

-----------------------
... create a supercell?
-----------------------

After defining your unitcell, you may want to create a larger supercell. This is pretty easy. In the following example
we want to create a ceria supercell of size 2x2x2:

.. code-block:: python

    >>> supercell = Lattice.from_lattice(ceria, 2, 2, 2)

As you see, the function :meth:`from_lattice` takes four arguments. The first one is the base lattice that is taken
as starting point. The three following values describe the size multiplier for each direction: x, y and z.

----------------------------
... select an atom?
----------------------------

The index-method of the lattice objects returns the atom from the atom list with the provided index:

>>> ceria[0]
Atom('Ce',FractionalVector([0.0, 0.0, 0.0]),'Ce')

You can also search for atoms with the :meth:`find`-method. You can specify the element, sublattice and the position
in fractinoal coordinates of the atoms to search for. Mind that only one argument is allowed for each criteria.
In addition you can specify the tolerance for the positional criteria in angstrom. If the distance between
the specified position and the atom is smaller than the tolerance, it will be found. Let's say that you want to find
the atom that is located at 0, 0, 0:

>>> caria.find(position=[0, 0, 0])
[Atom('Ce',FractionalVector([0.0, 0.0, 0.0]),'Ce')]

The return value is always a list, unless you set first=True. Then the first found atom is returned:

>>> print(ceria.find(position=[0, 0.5, 0.51]))
None

----------------------
... create a vacancy?
----------------------

To delete entries from the atom list of a lattice, you can use the :meth:`remove`-method. You need to specify
an atom that really is in the atom list of the lattice. Otherwise a :exc:`ValueError` is raised.

>>> supercell.remove(supercell[0])

-------------------------------------------------------
... get the distance between two points in the lattice?
-------------------------------------------------------

Every lattice object comes with a :meth:`distance` method. If you specify two points in fractional coordinates or
two atoms, it will return the distance between the two points in angstrom. This will take the periodic boundary
into consideration. You can turn this off by setting ```periodic=False```.

>>> supercell.distance([0,0,0], [0.8, 0.8, 0.8])
3.8101437239430154

>>> supercell.distance([0,0,0], [0.8, 0.8, 0.8], periodic=False)
15.240574895772065

-----------------------------------
... change the position of an atom?
-----------------------------------

:mod:`crystally` was created with the intend to manipulate lattices. Changing the position of an atom is therefore
very simple. First select the atom you would like to manipulate. Then change the :attr:`position` attribute:

.. code-block:: python

    >>> atom = lattice[0]
    >>> atom.position = [0.1, 0.1, 0.1]

--------------------------------------------
... increase the distance between two atoms?
--------------------------------------------

If you want to increase the distance of one point to a fixed center point you can either use :meth:`increase_distance_abs`
or :meth:`increase_distance_rel` from the :class:`Lattice` object. Both methods return the new position. With the first
method you can specify the distance increase as absolute value in angstrom. The second method increases the distance
on a relative basis.

Examples:
To increase the distance of a point at (0.5, 0.5, 0.5) from the origin by 1 Angstrom, do:

>>> print(supercell.increase_distance_abs([0,0,0], [0.5, 0.5, 0.5], 1))
[0.55249146 0.55249146 0.55249146]

--------------------------------------------
... read and write vasp files?
--------------------------------------------

To read a POSCAR or CONTCAR file, you can use the :func:`read_poscar` function. For example, if you would like to
read the file "POSCAR", type:

.. code-block:: python

    >>> lattice = crystally.read_poscar("POSCAR")

To write a :class:`Lattice` object to a POSCAR file, use the :func:`write_poscar` function. You need to specify the
:class:`Lattice` object, the name and location of the file and a short description for the header of the POSCAR file.

.. code-block:: python

    >>> cr.vasp.write_poscar(supercell, "POSCAR", "ceria supercell structure")