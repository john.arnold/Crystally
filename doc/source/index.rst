
Welcome to Crystally's documentation!
==========================================

:mod:`crystally` is a python library to analyze and manipulate crystal structures. It is intended to be easy to learn and
understand. Have a look at the :ref:`how_do_i` page to get started and an idea of the capabilities of the module.

In order to make the module as widely applicable as possible, I tried to keep :mod:`crystally` as clear, minimal and
extensible as possible. So if you have some complicated structural analysis this module can provide tools
to potentially achieve this goal more quickly.

I hope that :mod:`crystally` is as useful to you as it has been to me. ☺️

In order to understand crystally you should be familiar with the following conventions:

1. As an abbreviation for :mod:`crystally` we chose "cr" in examples, source code and tests.

2. Positions are always given as fractional coordinates.

3. Distances are always provided in Angstrom (:math:`10^{-10}` m)

4. Angles are given as radians.

5. Only 3D crystal structures are supported.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   how_do_i
   api_reference

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
