import unittest
import crystally as cr
import numpy as np
from numpy.linalg import det
import random
from crystally.core.constants import *

almost_equal_threshold = 1e-10


class TestFloatingPointRounding(unittest.TestCase):

    def test_const_rounding(self):
        a = 1.00000000000
        b = 0.99999999999
        self.assertTrue(round(a, FLOAT_PRECISION) == round(b, FLOAT_PRECISION))

    def test_const_rounding_neg(self):
        a = 1.0000
        b = 0.9999
        self.assertTrue(round(a, FLOAT_PRECISION) != round(b, FLOAT_PRECISION))


class TestFractionalVector(unittest.TestCase):

    def test_is_close(self):
        a = cr.PeriodicVector([0.4, 0.6, 0.8])
        b = np.array([0.4, 0.6, 0.8])
        diff = max(np.abs(a.value - b))
        self.assertTrue(diff < almost_equal_threshold)

    def test_periodicity(self):
        a = cr.PeriodicVector([0.4, 0.6, 0.8])
        b = cr.PeriodicVector([-0.6, 1.6, 0.8])
        diff = max(np.abs(a.value - b.value))
        self.assertTrue(diff < almost_equal_threshold)

    def test_addition(self):
        a = cr.PeriodicVector([0.4, 0.5, 0.6])
        test = a+a
        correct = np.array([0.8, 0.0, 0.2])
        diff = max(np.abs(test.value - correct))
        self.assertTrue(diff < almost_equal_threshold)

    def test_subtraction(self):
        a = cr.PeriodicVector([0.1, 0.5, 0.8])
        b = cr.PeriodicVector([0, 0, 0])
        c = cr.PeriodicVector([0.9, 0.5, 0.2])
        diff = max(np.abs((b-a).value - c.value))
        self.assertTrue(diff < almost_equal_threshold)

    def test_diff_func(self):
        a = cr.PeriodicVector([0.6, 0.9, 0.5])
        b = cr.PeriodicVector([0.4, 0.1, 0.5])
        c = np.array([-0.2, 0.2, 0.0])
        diff = max(np.abs(b.diff(a) - c))
        self.assertTrue(diff < almost_equal_threshold)

    def test_immutability(self):
        vec = cr.PeriodicVector([0.1, 0.1, 0.1])
        value = vec.value
        value[0] = 0.5
        self.assertTrue(value[0] != vec[0])


class TestLatticeGeometry(unittest.TestCase):

    ceria = cr.Lattice.from_lattice(cr.examples.ceria(), 2, 2, 2)

    ce_oxy_distance = 2.381339827464

    def test_distance1(self):
        distance = self.ceria.distance([0, 0, 0], [0.125, 0.125, 0.125])
        self.assertTrue(abs(distance - self.ce_oxy_distance) < almost_equal_threshold)

    def test_distance2(self):
        distance = self.ceria.distance([0, 0, 0], [-0.125, -0.125, -0.125])
        self.assertTrue(abs(distance - self.ce_oxy_distance) < almost_equal_threshold)

    def test_distance3(self):
        distance = self.ceria.distance(cr.Atom("", [0, 0, 0]), [0.125, 0.125, 0.125])
        self.assertTrue(abs(distance - self.ce_oxy_distance) < almost_equal_threshold)

    def test_rel_distance_increase(self):
        increase = 0.1  # increase of 10%
        new_pos = self.ceria.increase_distance_rel([0, 0, 0], [0.125, 0.125, 0.125], increase)
        distance = self.ceria.distance([0, 0, 0], new_pos)
        self.assertTrue(abs(distance - self.ce_oxy_distance*(1+increase)) < almost_equal_threshold)

        new_pos = self.ceria.increase_distance_rel([0, 0, 0], [-0.125, -0.125, 0.125], increase)
        distance = self.ceria.distance([0, 0, 0], new_pos)
        self.assertTrue(abs(distance - self.ce_oxy_distance*(1+increase)) < almost_equal_threshold)

        new_pos = self.ceria.increase_distance_rel([0, 0, 0], [0.875, 0.875, 0.125], increase)
        distance = self.ceria.distance([0, 0, 0], new_pos)
        self.assertTrue(abs(distance - self.ce_oxy_distance*(1+increase)) < almost_equal_threshold)

    def test_abs_distance_increase(self):
        increase = 0.5  # increase of 0.5 angstrom
        new_pos = self.ceria.increase_distance_abs([0, 0, 0], [0.125, 0.125, 0.125], increase)
        distance = self.ceria.distance([0, 0, 0], new_pos)
        self.assertTrue(abs(distance - (self.ce_oxy_distance + increase)) < almost_equal_threshold)

        new_pos = self.ceria.increase_distance_abs([0, 0, 0], [-0.125, -0.125, 0.125], increase)
        distance = self.ceria.distance([0, 0, 0], new_pos)
        self.assertTrue(abs(distance - (self.ce_oxy_distance + increase)) < almost_equal_threshold)

        new_pos = self.ceria.increase_distance_abs([0, 0, 0], [0.875, 0.875, 0.125], increase)
        distance = self.ceria.distance([0, 0, 0], new_pos)
        self.assertTrue(abs(distance - (self.ce_oxy_distance + increase)) < almost_equal_threshold)

    def test_angle_calculation(self):
        correct = 0.9553166181245093
        self.assertTrue(abs(correct - self.ceria.angle([0, 0.25, 0.25], [0.125, 0.125, 0.375], [0.125, 0.125, 0.125]))
                        < almost_equal_threshold)

    def test_polyhedron_volume1(self):
        correct = det(self.ceria.vectors)/(8*8)
        calc = self.ceria.polyhedron_volume([[0, 0, 0], [0.25, 0.25, 0.25], [0, 0.25, 0.25],
                                           [0.25, 0.25, 0], [0.25, 0, 0.25], [0, 0, 0.25],
                                           [0, 0.25, 0], [0.25, 0, 0]])
        self.assertTrue(abs(correct - calc) < almost_equal_threshold)

    def test_polyhedron_volume2(self):
        # check the volume of the Ce tetrahedron around oxygen
        distance_O_Ce = self.ceria.distance([0, 0, 0], [0.125, 0.125, 0.125])
        distance_Ce_Ce = self.ceria.distance([0, 0, 0], [0.25, 0.25, 0.0])

        correct = distance_Ce_Ce**3/(6*np.sqrt(2))

        vertices = list(self.ceria.get_in_radius([0.125, 0.125, 0.125], distance_O_Ce+0.1, 0.01))
        vertices = [x for x in vertices if x.element == "Ce"]
        calc = self.ceria.polyhedron_volume(vertices)
        self.assertTrue(abs(correct - calc) < almost_equal_threshold)

    def test_polyhedron_area(self):
        length_side = self.ceria.vectors[0, 0] / 4
        correct = 6*length_side**2
        calc = self.ceria.polyhedron_area([[0, 0, 0], [0.25, 0.25, 0.25], [0, 0.25, 0.25],
                                           [0.25, 0.25, 0], [0.25, 0, 0.25], [0, 0, 0.25],
                                           [0, 0.25, 0], [0.25, 0, 0]])
        self.assertTrue(abs(correct - calc) < almost_equal_threshold)

    def test_polyhedron_volume3(self):
        # check the area of the Ce tetrahedron around oxygen
        distance_O_Ce = self.ceria.distance([0, 0, 0], [0.125, 0.125, 0.125])
        distance_Ce_Ce = self.ceria.distance([0, 0, 0], [0.25, 0.25, 0.0])

        correct = np.sqrt(3) * distance_Ce_Ce ** 2

        vertices = list(self.ceria.get_in_radius([0.125, 0.125, 0.125], distance_O_Ce + 0.1, 0.01))
        vertices = [x for x in vertices if x.element == "Ce"]
        calc = self.ceria.polyhedron_area(vertices)
        self.assertTrue(abs(correct - calc) < almost_equal_threshold)


class TestLatticeLogic(unittest.TestCase):

    ceria = cr.Lattice.from_lattice(cr.examples.ceria(), 2, 2, 2)

    def test_get_index(self):
        index = 0
        atom = self.ceria[index]
        self.assertTrue(self.ceria.index(atom) == index)


class TestLatticeSorting(unittest.TestCase):

    def test_with_other_lattice(self):
        lattice1 = cr.Lattice.from_lattice(cr.examples.ceria(), 2, 2, 2)
        random.shuffle(lattice1.atoms)

        lattice2 = cr.Lattice.from_lattice(cr.examples.ceria(), 2, 2, 2)
        lattice1.sort(lattice2)

        for atom1, atom2 in zip(lattice1, lattice2):
            self.assertTrue(lattice1.distance(atom1.position, atom2.position) < 1e-10)

    def test_with_other_lattice_ignore_element(self):
        lattice1 = cr.Lattice.from_lattice(cr.examples.ceria(), 2, 2, 2)
        random.shuffle(lattice1.atoms)
        for atom in lattice1:
            atom.element = "XX"

        lattice2 = cr.Lattice.from_lattice(cr.examples.ceria(), 2, 2, 2)
        lattice1.sort(lattice2, ignore_element=True)

        for atom1, atom2 in zip(lattice1, lattice2):
            self.assertTrue(lattice1.distance(atom1.position, atom2.position) < 1e-10)

    def test_with_other_lattice_ignore_element_fail(self):
        lattice1 = cr.Lattice.from_lattice(cr.examples.ceria(), 2, 2, 2)
        random.shuffle(lattice1.atoms)
        for atom in lattice1:
            atom.element = "XX"

        lattice2 = cr.Lattice.from_lattice(cr.examples.ceria(), 2, 2, 2)
        lattice1.sort(lattice2)

        asserts = [lattice1.distance(atom1.position, atom2.position) < 1e-10
                   for atom1, atom2 in zip(lattice1, lattice2)]

        self.assertTrue(all(asserts) is False)


class TestRdfGeneration(unittest.TestCase):

    ceria = cr.Lattice.from_lattice(cr.examples.ceria(), 2, 2, 2)

    def test_neighbor_count(self):
        rdf = cr.generate_rdf(self.ceria, 3)
        for entry in rdf:
            if entry.center == "Ce" and entry.neighbor == "O":
                self.assertTrue(np.abs(entry.average_occurrence - 8) < FLOAT_ROUNDING)
            if entry.center == "O" and entry.neighbor == "Ce":
                self.assertTrue(np.abs(entry.average_occurrence - 4) < FLOAT_ROUNDING)
            if entry.center == "O" and entry.neighbor == "O":
                self.assertTrue(np.abs(entry.average_occurrence - 6) < FLOAT_ROUNDING)

    def test_distance_binning(self):
        rdf = cr.generate_rdf(self.ceria, 3, binning_fineness=1e-2)
        for entry in rdf:
            if entry.center == "Ce" and entry.neighbor == "O":
                self.assertTrue(np.abs(entry.distance - 2.38) < FLOAT_ROUNDING)
            if entry.center == "O" and entry.neighbor == "Ce":
                self.assertTrue(np.abs(entry.distance - 2.38) < FLOAT_ROUNDING)
            if entry.center == "O" and entry.neighbor == "O":
                self.assertTrue(np.abs(entry.distance - 2.74) < FLOAT_ROUNDING)
