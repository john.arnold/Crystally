import unittest
import crystally as cr
from pathlib import Path

almost_equal_threshold = 1e-10


class TestFileHandling(unittest.TestCase):

    def test_write_poscar(self):
        cr.vasp.write_poscar(cr.examples.ceria(), "POSCAR", "test")
        Path("POSCAR").unlink()

    def test_read_poscar(self):
        cr.vasp.write_poscar(cr.examples.ceria(), "POSCAR", "test")
        _ = cr.vasp.read_poscar("POSCAR")
        Path("POSCAR").unlink()


class TestTools(unittest.TestCase):

    def test_neb_make(self):
        lattice1 = cr.examples.ceria()
        lattice1.remove(lattice1[-1])
        lattice2 = cr.examples.ceria()
        lattice2.remove(lattice2[-2])

        neb_lattices = cr.vasp.make_neb(lattice1, lattice2)
        for i in range(len(lattice1.atoms) - 1):
            self.assertTrue(lattice1.distance(neb_lattices[0][i], neb_lattices[1][i]) < 0.001)
            self.assertTrue(lattice1.distance(neb_lattices[1][i], neb_lattices[2][i]) < 0.001)

        atom1 = cr.Atom(element="O",
                        position=neb_lattices[0][-1].position
                                 + neb_lattices[-1][-1].position.diff(neb_lattices[0][-1].position) / 2)

        atom2 = neb_lattices[1][-1]

        self.assertTrue(atom1.element == atom2.element)
        self.assertTrue(lattice1.distance(atom1, atom2) < 0.001)
